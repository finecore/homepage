import { list, item, post, put, del } from '@/api';
import { LIST, ITEM, COUNT, PAGE, mergeList } from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수

import _ from 'lodash';
import dayjs from 'dayjs';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { all_room_reservs }) {
      state.list = all_room_reservs;
    },
    [ITEM](state, { room_reserv }) {
      state.item = room_reserv[0] || room_reserv;
      mergeList(state.list, state.item, 'id');
    },
  },
  actions: {
    async getList({ state, commit, dispatch, loading = true }, { place_id }) {
      const promise = await list(
        dispatch,
        `room/reserv/all/${place_id}`,
        {}
      ).then(({ common: { success, error }, body: { all_room_reservs } }) => {
        if (success) {
          commit(LIST, {
            all_room_reservs,
          }); // page 는 화면에서 변경 되므로 clone 한다.
        }
        return { success, all_room_reservs };
      });
      return promise;
    },
    async getItem({ state, commit, dispatch, loading = true }, id) {
      const promise = await item(
        { dispatch, loading },
        `room/reserv/${id}`,
        {}
      ).then(({ common: { success, error }, body: { room_reserv } }) => {
        if (success) {
          commit(ITEM, { room_reserv });
        }
        return { success, error, room_reserv: room_reserv[0] || {} };
      });
      return promise;
    },
    async getItemByReservNum({ state, commit, dispatch, loading = true }, num) {
      const promise = await item(
        { dispatch, loading },
        `room/reserv/num/${num}`,
        {}
      ).then(({ common: { success, error }, body: { room_reserv } }) => {
        if (success) {
          commit(ITEM, { room_reserv });
        }
        return { success, error, room_reserv: room_reserv[0] || {} };
      });
      return promise;
    },
    async getItemByMmsNoNum({ state, commit, dispatch, loading = true }, num) {
      const promise = await item(
        { dispatch, loading },
        `room/reserv/mms/mo/${num}`,
        {}
      ).then(({ common: { success, error }, body: { room_reserv } }) => {
        if (success) {
          commit(ITEM, { room_reserv });
        }
        return { success, error, room_reserv: room_reserv[0] || {} };
      });
      return promise;
    },
    async newItem(
      { state, commit, dispatch, loading = true },
      room_reserv: any
    ) {
      const promise = await post({ dispatch, loading }, `room/reserv`, {
        room_reserv,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          room_reserv.id = info.insertId;
          commit(ITEM, { room_reserv });
        }
        return { success, error, room_reserv };
      });
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true },
      room_reserv: any
    ) {
      const promise = await put(
        { dispatch, loading },
        `room/reserv/${room_reserv.id}`,
        {
          room_reserv,
        }
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          commit(ITEM, { room_reserv });
        }
        return { success, error, room_reserv };
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }, id) {
      const promise = await del(dispatch, `room/reserv/${id}`, null).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            let room_reservs = state.list.filter((item) => item.id !== id);
            commit(LIST, { count: state.count - 1, room_reservs });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
