import {
  LIST,
  ITEM,
  COUNT,
  PAGE,
  SELECTED,
  PLACE_SEARCH_TEXT,
} from '@/store/mutation_types';
import { list, item, post, put, del } from '@/api';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import _ from 'lodash';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
    [SELECTED]: 0,
    [PLACE_SEARCH_TEXT]: '',
  },
  getters: {
    [COUNT](state: { [x: string]: any }) {
      return state[COUNT];
    },
    [LIST](state: { [x: string]: any }) {
      return state[LIST];
    },
    [ITEM](state: { [x: string]: any }) {
      return state[ITEM];
    },
    [SELECTED](state: { [x: string]: any }) {
      return state[SELECTED];
    },
    [PLACE_SEARCH_TEXT](state: { [x: string]: any }) {
      return state[PLACE_SEARCH_TEXT];
    },
  },
  mutations: {
    [LIST](
      state: { count: any; list: any; page: any },
      { count, list, page }: any
    ) {
      state.count = count;
      state.list = list;
      if (page) state.page = page;
    },
    [ITEM](state: { item: any; list: any[] }, { place }: any) {
      state.item = place;
    },
    [SELECTED](state: { [x: string]: any }, { id, text }: any) {
      if (id !== undefined) state[SELECTED] = id;
      if (text !== undefined) state[PLACE_SEARCH_TEXT] = text;
    },
    [PLACE_SEARCH_TEXT](state: { [x: string]: any }, text: any) {
      state[PLACE_SEARCH_TEXT] = text;
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true },
      {
        page,
        filter = '1=1',
        order = 'reg_date',
        desc = 'desc',
        limit = '10000',
      }
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(
        { dispatch, loading },
        `place/list/${filter}/${order}/${desc}/${limit}`,
        {}
      ).then(
        ({ common: { success, error }, body: { count, places: list } }) => {
          if (success) {
            commit(LIST, {
              count: count[0].count,
              list,
              page: _.cloneDeep(page),
            }); // page 는 화면에서 변경 되므로 clone 한다.
          } else {
            commit(LIST, []);
            return dispatch('setApiErr', error, { root: true });
          }
          return list;
        }
      );
      return promise;
    },
    async getItem({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await item({ dispatch, loading }, `place/${id}`, {}).then(
        ({ common: { success, error }, body: { place } }) => {
          if (success) {
            place = place[0] || place;
            commit(ITEM, { place });
          }
          return { success, error, place };
        }
      );
      return promise;
    },
    async newItem(
      { state, commit, dispatch, loading = true }: any,
      place: any
    ) {
      const promise = await post({ dispatch, loading }, `place`, {
        place,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          dispatch('getList', { page: state.page });
        }
        return success;
      });
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true }: any,
      place: { id: any }
    ) {
      const promise = await put({ dispatch, loading }, `place/${place.id}`, {
        place,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          let list = state.list.map((item: { id: any }) =>
            item.id !== place.id ? item : place
          );

          commit(LIST, { count: state.count, list });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await del(dispatch, `place/${id}`, null).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            dispatch('getList', { page: state.page });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
