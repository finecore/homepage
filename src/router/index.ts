import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'homeView',
    component: () => import('@/views/HomeView.vue'),
  },
  {
    path: '/main',
    name: 'mainView',
    component: () => import('@/views/main/Index.vue'),
    props: true,
  },
  {
    path: '/404',
    name: 'notFound',
    component: () => import('@/views/PageNotFound.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
