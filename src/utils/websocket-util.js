class WebSocketUtil {
  constructor(store, computed) {
    this.host = import.meta.env.VITE_APP_PAD_SOCKET_HOST;
    this.port = import.meta.env.VITE_APP_PAD_SOCKET_PORT;
    this.socket = null;
    this.status = 'disconnected';
    this.store = store;
    this.computed = computed;
    this.timer = null;

    console.log('- WebSocketUtil', this.host, this.port);
  }

  interval(timerDelay) {
    console.log('- WebSocketUtil interval ', { timerDelay });

    if (this.timer) clearInterval(this.timer);

    this.timer = setInterval(() => {
      const {
        auth: { user },
        place: { item }, // 관리 업소 목록.
        ws: { uuid },
      } = this.store.state;

      console.log('- WebSocketUtil onopen ', { user, item });

      // 웹소켓에 사용자 매핑(Socket Server)
      if (user && item) {
        this.sendMessage('JOIN_REQUESTED', {
          channel: 'pad',
          user,
          user_place: item,
        });

        if (uuid) {
          // uuid 가 없다면 추가.
          this.store.dispatch('addUuid', { uuid });

          this.interval(timerDelay * 100);
        }
      }
    }, timerDelay);
  }

  connect() {
    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    this.socket = new WebSocket(`ws://${this.host}:${this.port}`);

    console.log('- WebSocketUtil connect ', this.socket);

    this.socket.onopen = () => {
      const webSocket = this.socket;

      let {
        auth: { user },
      } = this.store.state;

      this.status = 'connected';

      console.log('- WebSocketUtil onopen ', this.status);

      if (this.status === 'connected') {
        this.store.dispatch('ws/setWebsocket', { webSocket });

        // 웹소켓에 사용자 매핑(Socket Server)
        this.interval(1000);
      }

      // websocket data receive.
      this.socket.onmessage = ({ data }) => {
        const message = data ? JSON.parse(data) : {};
        const { websocket = true, type = '', payload, metadata } = message;

        console.log(
          '%c -> websocket receive message ',
          'background: #222; color: #bada55',
          { type, payload }
        );

        if (type) {
          // let action = { websocket, type, ...payload, metadata }; // ...payload : reducer action 구조분해 할당.
          this.store.commit(type, { ...payload });
        }
      };

      // websocket data send.
      this.store.subscribe((mutation) => {
        if (mutation.type === 'UPDATE_DATA') {
          this.socket.emit('update', mutation.payload);
        }
      });
    };

    this.socket.onclose = () => {
      this.disconnect();

      setTimeout(() => {
        this.connect();
      }, 2000);
    };

    this.socket.onerror = (err) => {
      console.error('- WebSocketUtil onerror ', err);
    };
  }

  disconnect() {
    this.socket.close();
    this.status = 'disconnected';
    console.error('----> WebSocketUtil disconnected  ');
  }

  sendMessage(type, payload) {
    if (this.status !== 'connected') {
      console.error('----> WebSocketUtil disconnected  ');
      return;
    }
    console.log('----> WebSocketUtil send  ', type, payload);

    this.socket.send(JSON.stringify({ type, payload }));
  }
}

export default WebSocketUtil;
