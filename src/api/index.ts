import { HOST_URL } from './config';
import { api, ajax } from '@/utils/api-util';

// list
const list = async (config: any, url: string, data: any) => {
  const { dispatch, loading } = config;

  try {
    const res = await ajax.get(`${HOST_URL}/${url}`, { ...data, ...config });
    return api.checkResponse(dispatch, config, res);
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// item
const item = async (config: any, url: string, data: any) => {
  const { dispatch, loading } = config;
  try {
    const res = await ajax.get(`${HOST_URL}/${url}`, { ...data, ...config });
    return api.checkResponse(dispatch, config, res);
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// post
const post = async (config: any, url: string, data: any) => {
  const { dispatch, loading } = config;
  try {
    const res = await ajax.post(`${HOST_URL}/${url}`, data, config);
    return api.checkResponse(dispatch, config, res);
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// put
const put = async (config: any, url: string, data: any) => {
  const { dispatch, loading } = config;
  try {
    const res = await ajax.put(`${HOST_URL}/${url}`, data, config);
    return api.checkResponse(dispatch, config, res);
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// del
const del = async (config: any, url: string, data: any) => {
  const { dispatch, loading } = config;
  // data 를 넘기기 위해.
  try {
    const res = await ajax({
      url: `${HOST_URL}/${url}`,
      method: 'delete',
      data: { ...data },
      ...config,
    });
    return api.checkResponse(dispatch, config, res);
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

export { list, item, post, put, del };
