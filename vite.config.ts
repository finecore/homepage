import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';

// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin
import vuetify from 'vite-plugin-vuetify';

import { VitePWA } from 'vite-plugin-pwa';

export default defineConfig({
  plugins: [vue(), vuetify({ autoImport: true }), VitePWA()],
  server: {
    watch: {
      usePolling: true, // Hot Reload
    },
  },
  resolve: {
    alias: [
      { find: '@', replacement: path.resolve(__dirname, './src') },
      {
        find: '@components',
        replacement: path.resolve(__dirname, './src/components'),
      },
    ],
    extensions: ['.js', '.json', '.jsx', '.mjs', '.ts', '.tsx', '.vue'],
  },
  build: {
    /** If you set esmExternals to true, this plugins assumes that
     all external dependencies are ES modules */
    commonjsOptions: {
      esmExternals: true,
    },
  },
});
