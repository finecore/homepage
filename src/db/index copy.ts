import { join, dirname } from "path";
import { Low, LowSync, JSONFileSync, LocalStorage, JSONFile } from "lowdb";
import { fileURLToPath } from "url";

export const init = () => {
  console.log("lowdb init start!");

  // const __dirname = dirname(fileURLToPath(import.meta.url));

  type Data = {
    posts: object[]; // Expect posts to be an array of strings
  };

  // Use JSON file for storage
  const adapter = new JSONFileSync<Data>("db.json");
  const db = new LowSync<Data>(adapter);

  // Read data from JSON file, this will set db.data content
  db.read();

  db.data = db.data || { posts: [] }; // for node < v15.x

  // // You can also use this syntax if you prefer
  const { posts } = db.data;

  posts.push({ id: 1, title: "lowdb is awesome" });

  // // Write db.data content to db.json
  db.write();

  console.log("lowdb init success!");
};
