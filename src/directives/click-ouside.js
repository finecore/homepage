import ClickOutside from 'click-outside-vue3/src/v-click-outside';

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(ClickOutside, {});
});
