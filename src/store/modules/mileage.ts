import { list, item, post, put, del } from '@/api';
import { LIST, ITEM, COUNT, PAGE, mergeList } from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수

import _ from 'lodash';
import dayjs from 'dayjs';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state: { count: any }) {
      return state.count;
    },
    [LIST](state: { list: any }) {
      return state.list;
    },
    [ITEM](state: { item: any }) {
      return state.item;
    },
  },
  mutations: {
    [LIST](
      state: { count: any; list: any; page: any },
      { count, mileages, page }: any
    ) {
      state.count = count;
      state.list = mileages;
      if (page) state.page = page;
    },
    [ITEM](state: { item: any; list: any[] }, { mileage }: any) {
      state.item = mileage[0] || mileage;
      mergeList(state.list, state.item, 'id');
    },
  },
  actions: {
    async increaseMileage(
      { state, commit, dispatch, loading = true }: any,
      mileage: any
    ) {
      // rollback 1: 관리자 변경, 2: 고객 적립, 3: 고객 사용, 4: 적립 취소, 5: 사용 취소
      let { place_id, phone, user_id, point, rollback } = mileage;

      const promise = await put(
        { dispatch, loading },
        `mileage/increase/${place_id}/${phone}/${user_id}/${point}/${rollback}`,
        { mileage }
      ).then(({ common: { success, error }, body: { info, mileage } }) => {
        if (success) {
          commit(ITEM, { mileage });
        } else {
          commit(ITEM, []);
        }
        return { success, mileage: mileage[0] };
      });
      return promise;
    },

    async decreaseMileage(
      { state, commit, dispatch, loading = true }: any,
      mileage: any
    ) {
      let { place_id, phone, user_id, point, rollback } = mileage;

      const promise = await put(
        { dispatch, loading },
        `mileage/decrease/${place_id}/${phone}/${user_id}/${point}/${rollback}`,
        { mileage }
      ).then(({ common: { success, error }, body: { info, mileage } }) => {
        if (success) {
          commit(ITEM, { mileage });
        } else {
          commit(ITEM, []);
        }
        return { success, mileage: mileage[0] };
      });
      return promise;
    },

    async getList({ state, commit, dispatch, loading = true }, { place_id }) {
      const promise = await list(
        { dispatch, loading },
        `mileage/place/${place_id}`,
        {}
      ).then(({ common: { success, error }, body: { count, mileages } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            mileages,
          });
        } else {
          commit(LIST, []);
        }
        return { success, mileages };
      });
      return promise;
    },
    async getItem(
      { state, commit, dispatch, loading = true },
      { place_id, phone }
    ) {
      const promise = await item(
        { dispatch, loading },
        `mileage/${place_id}/phone/${phone}`,
        {}
      ).then(({ common: { success, error }, body: { mileage } }) => {
        if (success) {
          commit(ITEM, { mileage });
        } else {
          commit(ITEM, []);
        }
        return { success, mileage: mileage[0] };
      });
      return promise;
    },
    async newItem(
      { state, commit, dispatch, loading = true }: any,
      mileage: any
    ) {
      const promise = await post({ dispatch, loading }, `mileage`, {
        mileage,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          mileage.id = info.insertId;
          commit(ITEM, { mileage });
        }
        return success;
      });
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true },
      { place_id, phone, mileage }
    ) {
      const promise = await put(
        { dispatch, loading },
        `mileage/${place_id}/phone/${phone}`,
        {
          mileage,
        }
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          commit(ITEM, { mileage });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }, { id, userId }) {
      const promise = await del(dispatch, `mileage/${id}/${userId}`, null).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            let mileages = state.list.filter(
              (item: { id: any }) => item.id !== id
            );
            commit(LIST, { count: state.count - 1, mileages });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
