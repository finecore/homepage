import { list, item, post, put, del } from '@/api';
import { LIST, ITEM, COUNT, PAGE, mergeList } from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import _ from 'lodash';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1, size: 10 },
  },
  getters: {
    [COUNT](state: { count: any }) {
      return state.count;
    },
    [LIST](state: { list: any }) {
      return state.list;
    },
    [ITEM](state: { item: any }) {
      return state.item;
    },
  },
  mutations: {
    [LIST](
      state: { count: any; list: any; page: any },
      { count, preferencess, page }: any
    ) {
      state.count = count;
      state.list = preferencess;
      if (page) state.page = page;
    },
    [ITEM](state: { item: any; list: any[] }, { preferences }: any) {
      state.item = preferences[0] || preferences;
      mergeList(state.list, state.item, 'id');
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true },
      {
        page,
        filter = '1=1',
        order = 'reg_date',
        desc = 'desc',
        limit = '10000',
      }
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;

        limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(
        { dispatch, loading },
        `preferences/list/${filter}/${order}/${desc}/${limit}`,
        {}
      ).then(
        ({ common: { success, error }, body: { count, preferencess } }) => {
          if (success) {
            commit(LIST, {
              count: count[0].count,
              preferencess,
              page: _.cloneDeep(page),
            }); // page 는 화면에서 변경 되므로 clone 한다.
          } else {
            commit(LIST, []);
            return dispatch('setApiErr', error, { root: true });
          }
          return preferencess;
        }
      );
      return promise;
    },
    async getItem(
      { state, commit, dispatch, loading = true }: any,
      place_id: any
    ) {
      const promise = await item(
        { dispatch, loading },
        `preferences/${place_id}`,
        {}
      ).then(({ common: { success, error }, body: { preferences } }) => {
        if (success) {
          commit(ITEM, { preferences });
        } else {
          commit(ITEM, []);
          return dispatch('setApiErr', error, { root: true });
        }
        return preferences;
      });
      return promise;
    },
    async newItem(
      { state, commit, dispatch, loading = true }: any,
      preferences: any
    ) {
      const promise = await post({ dispatch, loading }, `preferences`, {
        preferences,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          dispatch('getList', { page: state.page });
        }
        return success;
      });
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true }: any,
      preferences: { id: any }
    ) {
      const promise = await put(
        { dispatch, loading },
        `preferences/${preferences.id}`,
        {
          preferences,
        }
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          let preferencess = state.list.map((item: { id: any }) =>
            item.id !== preferences.id ? item : preferences
          );

          commit(LIST, { count: state.count, preferencess });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await del(dispatch, `preferences/${id}`, null).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            dispatch('getList', { page: state.page });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
