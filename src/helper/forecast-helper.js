import axios from "axios";

/**
 * Forecast Helper Class.
 */
class ForecastHelper {
  constructor(socket) {
    // friendkj 운영계정 인증키 (일일 10만건)
    this.host = "http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2";
    this.serviceKey =
      "EEM%2FcDbR%2BPoguPKjXAgxq%2B8%2FH%2BRiv%2BFwO0r%2BcVzN9T3XoxSqrE2US3kicy%2FADcKle8%2Fg9DfzxcFUSD0SQTlztg%3D%3D";
    /*
    초단기실황조회 getForecastGrib
    초단기예보조회 getForecastTimeData
    동네예보조회 getForecastSpaceData
    예보버전조회 getForecastVersionCheck
    */

    /*
    위경도 - 기상청 격자 맵핑
    기상청은 전국을 5km×5km 간격의 촘촘한 격자화하여 읍,면,동 단위로 상세한 날씨를 제공하는 동네예보를 제공합니다.
    구역별 기상데이터를 관리하기 위해 한반도를 가로로 149개, 세로로 253개의 선을 그어 그리드형태로 관리하며, 위경도 데이터를 이 그리드 상의 좌표로 변화하는 알고리즘을 제공하고 있습니다.
    */
    this.NX = 149; // X축 격자점 수
    this.NY = 253; // Y축 격자점 수

    this.Re = 6371.00877; //  지도반경
    this.grid = 5.0; //  격자간격 (km)
    this.slat1 = 30.0; //  표준위도 1
    this.slat2 = 60.0; //  표준위도 2
    this.olon = 126.0; //  기준점 경도
    this.olat = 38.0; //  기준점 위도
    this.xo = 210 / this.grid; //  기준점 X좌표
    this.yo = 675 / this.grid; //  기준점 Y좌표

    this.PI = Math.asin(1.0) * 2.0;
    this.DEGRAD = this.PI / 180.0;
    this.RADDEG = 180.0 / this.PI;

    this.re = this.Re / this.grid;
    this.slat1 = this.slat1 * this.DEGRAD;
    this.slat2 = this.slat2 * this.DEGRAD;
    this.olon = this.olon * this.DEGRAD;
    this.olat = this.olat * this.DEGRAD;

    this.sn =
      Math.tan(this.PI * 0.25 + this.slat2 * 0.5) / Math.tan(this.PI * 0.25 + this.slat1 * 0.5);
    this.sn = Math.log(Math.cos(this.slat1) / Math.cos(this.slat2)) / Math.log(this.sn);
    this.sf = Math.tan(this.PI * 0.25 + this.slat1 * 0.5);
    this.sf = (Math.pow(this.sf, this.sn) * Math.cos(this.slat1)) / this.sn;
    this.ro = Math.tan(this.PI * 0.25 + this.olat * 0.5);
    this.ro = (this.re * this.sf) / Math.pow(this.ro, this.sn);
    this.first = 1;
  }

  // 위경도 -> 그리드.
  mapToGrid(lat, lon, code = 0) {
    let ra = Math.tan(this.PI * 0.25 + lat * this.DEGRAD * 0.5);
    ra = (this.re * this.sf) / Math.pow(ra, this.sn);
    let theta = lon * this.DEGRAD - this.olon;
    if (theta > this.PI) theta -= 2.0 * this.PI;
    if (theta < -this.PI) theta += 2.0 * this.PI;
    theta *= this.sn;
    let x = ra * Math.sin(theta) + this.xo;
    let y = this.ro - ra * Math.cos(theta) + this.yo;
    x = Number.parseInt(x + 1.5);
    y = Number.parseInt(y + 1.5);
    return { x, y };
  }

  // 그리드 -> 위경도.
  gridToMap(x, y, code = 1) {
    x = x - 1;
    y = y - 1;
    let xn = x - this.xo;
    let yn = this.ro - y + this.yo;
    let ra = Math.sqrt(xn * xn + yn * yn);
    if (this.sn < 0.0) ra = -ra;
    let alat = Math.pow((this.re * this.sf) / ra, 1.0 / this.sn);
    alat = 2.0 * Math.atan(alat) - this.PI * 0.5;

    let theta = 0.0;
    if (Math.abs(xn) <= 0.0) theta = 0.0;
    else if (Math.abs(yn) <= 0.0) theta = this.PI * 0.5;
    if (xn < 0.0) theta = -theta;
    else theta = Math.atan2(xn, yn);
    let alon = theta / this.sn + this.olon;
    let lat = alat * this.RADDEG;
    let lon = alon * this.RADDEG;

    return { lat, lon };
  }

  // cors 크로스 도메인 문제로 api 서버에서 조회 해오는 방법으로 변경.
  // get(url, params, callback, error) {
  //   params.serviceKey = decodeURIComponent(this.serviceKey);

  //   console.log("- forecast url", this.host + `/${url}`, params);

  //   axios
  //     .get(this.host + `/${url}`, {
  //       params,
  //       headers: {
  //         "Content-Type": "application/json"
  //       }
  //     })
  //     .then(result => {
  //       const {
  //         status,
  //         statusText,
  //         data: {
  //           response: { header, body }
  //         }
  //       } = result;

  //       console.log("- forecast result", { status, statusText, header, body });

  //       if (header.resultCode === "0000") {
  //         if (callback) callback(null, body);
  //       } else {
  //         console.error("날씨정보 조회 실패! [" + header.resultCode + "] " + header.resultMsg);
  //         const err = { code: header.resultCode, message: header.resultMsg };
  //         if (callback) callback(err, null);
  //       }
  //     })
  //     .catch(result => {
  //       console.error(result);
  //       const { status, responseText } = result;
  //       const err = { code: status || 9999, message: responseText || result.message };
  //       if (callback) callback(err, null);
  //     });
  // }
}

export default ForecastHelper;
