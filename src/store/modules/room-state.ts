import { list, item, post, put, del } from '@/api';
import { LIST, ITEM, COUNT, PAGE, mergeList } from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import _ from 'lodash';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state: { [x: string]: any }) {
      return state[COUNT];
    },
    [LIST](state: { [x: string]: any }) {
      return state[LIST];
    },
    [ITEM](state: { [x: string]: any }) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](
      state: { count: any; list: any; page: any },
      { count, room_states, page }: any
    ) {
      state.list = room_states;
    },
    [ITEM](state: { item: any; list: any[] }, { room_state }: any) {
      state.item = room_state[0] || room_state;
      mergeList(state.list, state.item, 'id');
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true },
      {
        page,
        filter = '1=1',
        order = 'a.room_id',
        desc = 'asc',
        limit = '10000',
      }
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(
        { dispatch, loading },
        `room/state/list/${filter}/${order}/${desc}/${limit}`,
        {}
      ).then(({ common: { success, error }, body: { count, room_states } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            room_states,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
          return dispatch('setApiErr', error, { root: true });
        }
        return room_states;
      });
      return promise;
    },
    async getItem(
      { state, commit, dispatch, loading = true }: any,
      room_id: number
    ) {
      const promise = await item(
        { dispatch, loading },
        `room/state/${room_id}`,
        {}
      ).then(({ common: { success, error }, body: { room_state } }) => {
        if (success) {
          room_state = room_state[0] || room_state;
          commit(ITEM, { room_state });
        }
        return { success, error, room_state };
      });
      return promise;
    },
    async getItemByroomId(
      { state, commit, dispatch, loading = true }: any,
      room_id: any
    ) {
      const promise = await item(
        { dispatch, loading },
        `room/state/sale/${room_id}`,
        {}
      ).then(({ common: { success, error }, body: { room_state } }) => {
        if (success) {
          room_state = room_state[0] || room_state;
          commit(ITEM, { room_state });
        }
        return { success, error, room_state };
      });
      return promise;
    },
    async newItem(
      { state, commit, dispatch, loading = true }: any,
      room_state: any
    ) {
      const promise = await post({ dispatch, loading }, `room/state`, {
        room_state,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          let room_states = state.list.concat(room_state);
          commit(LIST, { count: state.count++, room_states });
        }
        return { success, error };
      });
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true }: any,
      room_state: any
    ) {
      console.log('- setItem', { room_state });

      const promise = await put(
        { dispatch, loading },
        `room/state/${room_state.room_id}`,
        {
          room_state,
        }
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          commit(ITEM, { room_state });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await del(dispatch, `room/state/${id}`, null).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            let room_states = state.list.filter((item: any) => item.id !== id);
            commit(LIST, { count: state.count--, room_states });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
