import { join, dirname } from "path";
import { Low, LowSync, JSONFileSync, LocalStorage, JSONFile } from "lowdb";
import { fileURLToPath } from "url";

const init = async () => {
  console.log("lowdb init start!");

  // const __dirname = dirname(fileURLToPath(import.meta.url));

  // Use JSON file for storage
  const adapter = new JSONFileSync("db.json");
  const db = new LowSync(adapter);

  // Read data from JSON file, this will set db.data content
  await db.read();

  // // If file.json doesn't exist, db.data will be null
  // // Set default data
  // db.data ||= { posts: [] }
  db.data = db.data || { posts: [] }; // for node < v15.x
  // // Create and query items using plain JS
  // db.data.posts.push('hello world')
  // db.data.posts[0]

  // // You can also use this syntax if you prefer
  const { posts } = db.data;

  posts.push({ id: 1, title: "lowdb is awesome" });

  // // Write db.data content to db.json
  await db.write();

  console.log("lowdb init success!");
};

export { init };
