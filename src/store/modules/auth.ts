import {
  USER,
  IS_LOGINED,
  TOKEN,
  LOGIN_ID,
  DUP_ACCESS_LOGOUT,
  DUP_ACCESS_FAILUE,
  IS_SUPERVISOR,
  IS_ADMIN,
  IS_VIEWER,
  IS_ALIVE,
} from '@/store/mutation_types';

import { post, item } from '@/api';
import router from '@/router/index';

import wsAction from './ws';

function ParseJsonString(str: string) {
  try {
    return JSON.parse(str);
  } catch (e) {}
}

export default {
  state: {
    // 화면 reload 시 로그인 정보 재설정 한다.(로그인 정보만 재설정)
    [USER]: ParseJsonString(sessionStorage.getItem('user')),
    [TOKEN]: sessionStorage.getItem(TOKEN),
    [IS_LOGINED]: sessionStorage.getItem(IS_LOGINED) === 'true',
    [IS_ALIVE]: false,
    [DUP_ACCESS_LOGOUT]: false,
    [DUP_ACCESS_FAILUE]: false,
  },
  getters: {
    [USER](state: { [x: string]: any }) {
      return state[USER] || { id: '', pwd: '' };
    },
    [TOKEN](state: { [x: string]: any }) {
      return state[TOKEN];
    },
    [IS_LOGINED](state: { [x: string]: any }) {
      return state[IS_LOGINED];
    },
    [IS_ALIVE](state: { [x: string]: any }) {
      return state[IS_ALIVE];
    },
    [IS_SUPERVISOR](state: { [x: string]: { type: any; level: any } }) {
      if (state[IS_LOGINED]) {
        const { type, level } = state[USER];
        return type === 1 && level === 0;
      }
      return false;
    },
    [IS_ADMIN](state: { [x: string]: { type: any; level: any } }) {
      if (state[IS_LOGINED]) {
        const { type, level } = state[USER];
        return type === 1 && level < 3;
      }
      return false;
    },
    [IS_VIEWER](state: { [x: string]: { type: any; level: any } }) {
      if (state[IS_LOGINED]) {
        const { type, level } = state[USER];
        return type === 1 && level === 9;
      }
      return false;
    },
    [DUP_ACCESS_LOGOUT](state: { [x: string]: any }) {
      return state[DUP_ACCESS_LOGOUT];
    },
    [DUP_ACCESS_FAILUE](state: { [x: string]: any }) {
      return state[DUP_ACCESS_FAILUE];
    },
  },
  mutations: {
    [USER](state: { [x: string]: boolean; toke: any }, { user, token }: any) {
      state[USER] = user;
      state[IS_LOGINED] = true;
      if (token) state.toke = token;
    },
    [IS_LOGINED](state: { [x: string]: any }, isUSER: any) {
      state[IS_LOGINED] = isUSER;
    },
    [IS_ALIVE](state: { [x: string]: any }, alive: any) {
      state[IS_ALIVE] = alive;
    },
    // 현재 사용 안함 추후 동시 접속 라이선스 기능 사용 시 적용.
    [DUP_ACCESS_LOGOUT](state: { [x: string]: any }, isUSER: any) {
      state[DUP_ACCESS_LOGOUT] = isUSER;
    },
    // 현재 사용 안함 추후 동시 접속 라이선스 기능 사용 시 적용.
    [DUP_ACCESS_FAILUE](state: { [x: string]: any }, isUSER: any) {
      state[DUP_ACCESS_FAILUE] = isUSER;
    },
  },
  actions: {
    async login({ state, commit, dispatch, loading = true }, { id, pwd }) {
      // console.log('- actions login', { state, commit, dispatch, loading });

      // call api.
      const promise = await post({ dispatch, loading }, 'login', {
        id,
        pwd,
      }).then(
        (
          result = {
            common: {
              success: false,
              error: null,
            },
            body: { user: {}, token: '' },
          }
        ) => {
          // console.log('-  login result', result);

          let {
            common: { success, error },
            body: { user, token },
          } = result;

          if (success) {
            commit(USER, { user, token });
          } else {
            commit(USER, { user: null, token: null });
          }

          commit(IS_LOGINED, success);

          // 세션 스토리지 저장.
          sessionStorage.setItem(IS_LOGINED, JSON.stringify(true));
          sessionStorage.setItem(USER, JSON.stringify(user)); // user 정보 저장.
          sessionStorage.setItem(TOKEN, token); // token 정보 저장.
          localStorage.setItem(LOGIN_ID, id); // 최종 로그인 id 정보 로컬 저장.

          return { success, user };
        }
      );
      return promise;
    },

    async addUuid({ state, commit, dispatch, loading = true }: any, { uuid }) {
      commit(USER, { user: { ...state.user, uuid } });
    },

    async logout({ state, commit, dispatch, loading = true }) {
      commit(USER, { user: null, token: null });
      commit(IS_LOGINED, null);

      sessionStorage.removeItem(IS_LOGINED);
      sessionStorage.removeItem(USER); // user 정보 삭제.
      sessionStorage.removeItem(TOKEN); // token 정보 저장.

      router.push('/idle');

      return true;
    },

    async isAlive({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await item(
        { dispatch, loading: false },
        `alive`,
        {}
      ).then(({ common: { success, error }, body: { alive } }) => {
        if (success) {
          commit(IS_ALIVE, { alive });
        }
        return { success, error, alive };
      });
      return promise;
    },
  },
};
