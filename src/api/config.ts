const debug = process.env.NODE_ENV !== 'production';

const HOST = import.meta.env.VITE_APP_API_SERVER_HOST;
const HOST_NGROK = import.meta.env.VITE_APP_API_SERVER_HOST_NGROK;
const PORT = import.meta.env.VITE_APP_API_SERVER_PORT;

const DOMAIN = window.location.hostname;
const PROTOCOL = window.location.protocol;

const HOST_URL = PROTOCOL === 'http:' ? HOST + ':' + PORT : HOST_NGROK; // ngrok 는 port 럾다

console.log(
  '-> API Server ',
  DOMAIN,
  PROTOCOL,
  import.meta.env.VITE_MODE,
  HOST_URL,
  ' debug:',
  debug
);

export { HOST, PORT, HOST_URL };
