import { list, item, post, put, del } from '@/api';
import {
  LIST,
  ITEM,
  COUNT,
  PAGE,
  SELECTED,
  mergeList,
} from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import _ from 'lodash';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    count: 0,
    list: [],
    item: {},
    page: { no: 1 },
  },
  getters: {
    [COUNT](state: { [x: string]: any }) {
      return state[COUNT];
    },
    [LIST](state: { [x: string]: any }) {
      return state[LIST];
    },
    [ITEM](state: { [x: string]: any }) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](
      state: { [x: string]: any; count: any; page: any },
      { count, users, page }: any
    ) {
      state.count = count;
      state[LIST] = users;
      if (page) state.page = page;
    },
    [ITEM](state: { [x: string]: any }, { user }: any) {
      state[ITEM] = user;
      mergeList(state[LIST], state[ITEM], 'id');
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true },
      { page, filter = '1=1', order = 'id', desc = 'asc', limit = '10000' }
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(
        { dispatch, loading },
        `user/list/${filter}/${order}/${desc}/${limit}`,
        {}
      ).then(({ common: { success, error }, body: { count, users } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            users,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
          return dispatch('setApiErr', error, { root: true });
        }
        return users;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await item({ dispatch, loading }, `user/${id}`, {}).then(
        ({ common: { success, error }, body: { user } }) => {
          if (success) {
            commit(ITEM, { user });
          } else {
            commit(ITEM, []);
            return dispatch('setApiErr', error, { root: true });
          }
          return user;
        }
      );
      return promise;
    },
    async newItem({ state, commit, dispatch, loading = true }: any, user: any) {
      const promise = await post({ dispatch, loading }, `user`, { user }).then(
        ({ common: { success, error }, body: { info, user } }) => {
          if (success) {
            dispatch('getList', { page: 1 });
          }
          return { success, user };
        }
      );
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true }: any,
      user: { id: any }
    ) {
      const promise = await put({ dispatch, loading }, `user/${user.id}`, {
        user,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          commit(ITEM, { user });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await del(dispatch, `user/${id}`, null).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            dispatch('getList', { page: 1 });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
