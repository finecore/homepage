import { LIST, ITEM, COUNT } from '@/store/mutation_types';
import { list, item, post, put, del } from '@/api';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import _ from 'lodash';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    count: 0,
    list: [],
    item: {},
    page: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](state, { count, list, page }) {
      state.count = count;
      state.list = list;
      if (page) state.page = page;
    },
    [ITEM](state, item) {
      state.item = item;
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true },
      {
        page,
        filter = '1=1',
        order = 'reg_date',
        desc = 'desc',
        limit = '10000',
      }
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(
        { dispatch, loading },
        `company/list/${filter}/${order}/${desc}/${limit}`,
        {}
      ).then(
        ({ common: { success, error }, body: { count, companys: list } }) => {
          if (success) {
            commit(LIST, {
              list,
              page: _.cloneDeep(page),
            }); // page 는 화면에서 변경 되므로 clone 한다.
          } else {
            commit(LIST, []);
            return dispatch('setApiErr', error, { root: true });
          }
          return list;
        }
      );
      return promise;
    },
    async getItem({ state, commit, dispatch, loading = true }, id) {
      const promise = await item(
        { dispatch, loading },
        `company/${id}`,
        {}
      ).then(({ common: { success, error }, body: { company: item } }) => {
        if (success) {
          commit(ITEM, item);
        } else {
          commit(ITEM, []);
          return dispatch('setApiErr', error, { root: true });
        }
        return item;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch, loading = true }, company) {
      const promise = await post({ dispatch, loading }, `company`, {
        company,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          dispatch('getList', { page: state.page });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch, loading = true }, company) {
      const promise = await put(
        { dispatch, loading },
        `company/${company.id}`,
        {
          company,
        }
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          let list = state.list.map((item) =>
            item.id !== company.id ? item : company
          );

          commit(LIST, { count: state.count, list });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }, id) {
      const promise = await del(
        { dispatch, loading },
        `company/${id}`,
        {}
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          dispatch('getList', { page: state.page });
        }
        return success;
      });
      return promise;
    },
  },
};
