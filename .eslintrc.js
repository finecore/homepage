/** @format */

module.exports = {
  root: true,
  env: {
    node: true,
  },
   extends : [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:prettier/recommended",
    "plugin:vue/vue3-essential",
    "prettier/@typescript-eslint"
  ],
  parserOptions: {
    "parser": "@babel/eslint-parser",
    ecmaVersion: 2020,
  },
  rules: {
    "prettier/prettier": [
      "error",
      {
        endOfLine: false,
      },
    ],
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    quotes: "off",
    semi: "off",
    curly: "off",
    "space-before-function-paren": "off",
    camelcase: "off",
    "brace-style": "off",
    indent: "off",
    "no-tabs": "off",
    "no-unused-vars": "off",
    "handle-callback-err": "off",
    "spaced-comment": "off",
    "no-throw-literal": "off",
    "no-unused-expressions": "off",
    "no-sequences": "off",
    "comma-dangle": "off",
    "new-cap": "off",
  },
};
