import { list, item, post, put, del } from '@/api';
import { LIST, ITEM, COUNT, PAGE, mergeList } from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import dayjs from 'dayjs';
import _ from 'lodash';

export default {
  namespaced: true, // namespaced instead namespace
  state: {},
  getters: {},
  mutations: {},
  actions: {
    async totalPlaceSido({ state, commit, dispatch, loading = true }, args) {
      const promise = await list(
        { dispatch, loading },
        `main/total/place/sido`,
        {
          ...args,
        }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async totalNowIeg({ state, commit, dispatch, loading = true }, args) {
      const promise = await list({ dispatch, loading }, `main/total/now/ieg`, {
        ...args,
      }).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async listNowIeg({ state, commit, dispatch, loading = true }, args) {
      let {
        filter = '1=1',
        order = 'reg_date',
        desc = 'desc',
        limit = '10000',
      } = args;

      const promise = await list(
        dispatch,
        `main/list/now/ieg/${filter}/${order}/${desc}/${limit}`,
        { loading }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async totalNowIsg({ state, commit, dispatch, loading = true }, args) {
      const promise = await list({ dispatch, loading }, `main/total/now/isg`, {
        ...args,
      }).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async listNowIsg({ state, commit, dispatch, loading = true }, args) {
      let {
        filter = '1=1',
        order = 'reg_date',
        desc = 'desc',
        limit = '10000',
      } = args;

      const promise = await list(
        dispatch,
        `main/list/now/isg/${filter}/${order}/${desc}/${limit}`,
        { loading }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async totalReservSum({ state, commit, dispatch, loading = true }, args) {
      let {
        filter = '1=1',
        begin = dayjs().format('YYYY-MM-DD'),
        end = dayjs().format('YYYY-MM-DD'),
        type = 'day',
        groups = 'stay_type',
      } = args;

      const promise = await list(
        dispatch,
        `main/total/reserv/sum/${filter}/${begin}/${end}/${type}/${groups}`,
        { loading }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async totalRoomStatusSum(
      { state, commit, dispatch, loading = true },
      args
    ) {
      let { filter = '1=1', groups = 'signal' } = args;

      const promise = await list(
        dispatch,
        `main/total/room/status/sum/${filter}/${groups}`,
        { loading }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async totalIsgStatusSum({ state, commit, dispatch, loading = true }, args) {
      let { filter = '1=1', groups = 'state' } = args;

      const promise = await list(
        dispatch,
        `main/total/isg/status/sum/${filter}/${groups}`,
        { loading }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async nowEventList({ state, commit, dispatch, loading = true }, args) {
      let { filter = '1=1', limit = 100 } = args;

      const promise = await list(
        dispatch,
        `main/list/now/event/${filter}/${limit}`,
        { loading }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async totalReportIeg({ state, commit, dispatch, loading = true }, args) {
      let {
        filter = '1=1',
        begin = dayjs().format('YYYY-MM-DD'),
        end = dayjs().format('YYYY-MM-DD'),
        type = 'month',
        groups = 'a.reg_date',
      } = args;

      const promise = await list(
        dispatch,
        `main/total/report/ieg/${filter}/${begin}/${end}/${type}/${groups}`,
        { ...args }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
    async totalReportIsg({ state, commit, dispatch, loading = true }, args) {
      let {
        filter = '1=1',
        begin = dayjs().format('YYYY-MM-DD'),
        end = dayjs().format('YYYY-MM-DD'),
        type = 'month',
        groups = 'a.reg_date',
      } = args;

      const promise = await list(
        dispatch,
        `main/total/report/isg/${filter}/${begin}/${end}/${type}/${groups}`,
        { ...args }
      ).then(({ common: { success, error }, body: { result } }) => {
        return result;
      });
      return promise;
    },
  },
};
