import { list, item, post, put, del } from '@/api';
import { LIST, ITEM, COUNT, PAGE, mergeList } from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수

import _ from 'lodash';
import dayjs from 'dayjs';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](
      state: { count: any; list: any; page: any },
      { count, room_types, page }: any
    ) {
      state.list = room_types;
    },
    [ITEM](state: { item: any; list: any[] }, { room_type }: any) {
      state.item = room_type[0] || room_type;
      mergeList(state.list, state.item, 'id');
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true }: any,
      place_id: any
    ) {
      const promise = await list(
        dispatch,
        `room/type/all/${place_id}`,
        {}
      ).then(({ common: { success, error }, body: { all_room_types } }) => {
        if (success) {
          commit(LIST, {
            all_room_types,
          }); // page 는 화면에서 변경 되므로 clone 한다.
        }
        return all_room_types;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch, loading = true }: any, id: any) {
      const promise = await item(
        { dispatch, loading },
        `room/type/${id}`,
        {}
      ).then(({ common: { success, error }, body: { room_type } }) => {
        if (success) {
          commit(ITEM, { room_type });
        }
        return { success, error, room_type: room_type[0] || {} };
      });
      return promise;
    },
    async newItem({ state, commit, dispatch, loading = true }, room_type) {
      const promise = await post({ dispatch, loading }, `room/type`, {
        room_type,
      }).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          room_type.id = info.insertId;
          commit(ITEM, { room_type });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch, loading = true }, room_type) {
      const promise = await put(
        { dispatch, loading },
        `room/type/${room_type.id}`,
        {
          room_type,
        }
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          commit(ITEM, { room_type });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch, loading = true }, id) {
      const promise = await del(dispatch, `room/type/${id}`, null).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            let room_types = state.list.filter((item) => item.id !== id);
            commit(LIST, { count: state.count - 1, room_types });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
