import { createApp } from 'vue';

import './styles/styles.scss';

import App from './App.vue';
import router from './router';
import store from './store';

import vuetify from './plugins/vuetify';
import VuetifyMoney from 'vuetify-money';
import { loadFonts } from './plugins/webfontloader';

import mitt from 'mitt';

import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import isoWeek from 'dayjs/plugin/isoWeek';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';

import format from '@/utils/format-util';

// import 'dayjs/locale/ko';
// dayjs.locale('ko');

dayjs.extend(isoWeek);
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(isSameOrBefore);

const emitter = mitt();

loadFonts();

let Vue = createApp(App);

Vue.use(VuetifyMoney);

Vue.config.globalProperties.$format = format;
Vue.config.globalProperties.$emitter = emitter;

Vue.provide('emitter', emitter);

Vue.use(router).use(store).use(vuetify);

Vue.mount('#app');
