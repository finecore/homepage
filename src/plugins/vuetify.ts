// Styles
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/styles';

// Vuetify
import { createVuetify } from 'vuetify';
import { VDatePicker } from 'vuetify/labs/VDatePicker';

export default createVuetify({
  components: {
    VDatePicker,
  },
});
// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
