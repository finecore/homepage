import axios, { AxiosInstance } from 'axios';
import { SET_API_ERR } from '@/store/mutation_types';
import dayjs from 'dayjs';
import _ from 'lodash';

let fetching = false;

const api = {
  // error request.
  errorRequest: (dispatch: any, err: any) => {
    if (!err?.response) return;

    console.error('- errorRequest', err, '\n err.response', err.response);

    let error: any = {
      code: 'Network Error',
      message: 'HttpRequest ERR_CONNECTION_REFUSED',
      detail: '',
    };

    if (err.response) {
      const comerr: any = err.response.data.common
        ? err.response.data.common.error
        : error;

      error = {
        code: err.response.status,
        message: err.response.statusText,
        detail: comerr.message,
      };
    } else if (err) {
      const errs = String(err).split(':');

      error = {
        type: SET_API_ERR,
        code: errs[0] === 'TypeError' ? 401 : 400,
        message: errs[1] ? errs[1] : err,
      };
    }

    dispatch('setApiErr', error, { root: true });
    dispatch('hideLoader', null, { root: true });

    fetching = false;

    return err;
  },

  // check response.
  checkResponse: (dispatch: any, config: any, res: any) => {
    // console.log('- checkResponse', res);

    let { common, body = {} } = res.data;

    let { success = false, error = {} } = common;

    // console.log('-> ', { success, error });

    if (res.status !== 200) {
      error = {
        type: SET_API_ERR,
        code: res.statue,
        message: res?.data?.message || '',
        detail: res.statusText,
      };

      console.error('- checkResponse', error);
      dispatch('setApiErr', error, { root: true });
    } else if (!success) {
      if (error.detail && !(error.detail instanceof Array))
        error.detail = [error.detail];
      if (res.status !== 200 && !error.detail)
        error.detail.push(res.statusText);

      console.error('- checkResponse', error);
      dispatch('setApiErr', error, { root: true });
    }

    fetching = false;

    return { common, body }; // return object.
  },
};

const getFormData = (json: any) => {
  console.log('----------------- form data -----------------');
  const formData = new FormData();
  for (const key in json) {
    // console.log(key, json[key]);
    formData.append(key, json[key]);
  }
  for (const key of formData.entries()) console.log('- ', key.join(':'));
  console.log('----------------- form data -----------------');
  return formData;
};

// Ajax As Axios
const ajax: AxiosInstance = axios.create({
  baseURL: '/',
  headers: {
    'Content-type': 'application/json',
  },
});

// Add a request interceptor
ajax.interceptors.request.use(
  (config: any) => {
    const { headers, url, method, dispatch, data, loading } = config;

    // if (fetching) {
    //   return Promise.reject(config);
    // }

    // console.log('- ajax request config ', { ...config });

    // 로드 시작.
    fetching = true;

    // 서버 모드 전송
    headers.server_mode = import.meta.env.VITE_MODE;

    const token = sessionStorage.getItem('token');

    if (token) {
      headers['x-access-token'] = token;
    } else if (url.indexOf('/login') === -1 && !token) {
      console.error('토큰이 필요 합니다.');
      return Promise.reject(config);
    }

    const uuid = sessionStorage.getItem('uuid');
    if (uuid) headers.uuid = uuid;

    // 채널 정보 전송
    headers.channel = 'pad';

    // console.log('- ajax request config ', { ...config });

    config.url = url;
    config.time = new Date();

    // file POST 는 form data 로 해야 파라메터 전송됨!(multipart/form-data 일때만 파라메터 전송)
    if (headers['Content-Type'] === 'multipart/form-data') {
      config.data = getFormData(data);
    }

    console.log(
      '>>>>>> axios request >>>>>>\n %c' + method.toUpperCase(),
      'background: #222; color: #bada55',
      url,
      dayjs(config.time).format('mm:ss:SSS')
    );
    if (data)
      console.log(
        'data: %c' + JSON.stringify(data, null, 2),
        'background: #ececb8; color: blue'
      );

    if (dispatch) {
      // 로딩바 보이기.
      if (loading !== false) dispatch('showLoader', null, { root: true });
    }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// Add a response interceptor
ajax.interceptors.response.use(
  (response: any) => {
    let diff = dayjs
      .utc(
        dayjs(new Date(), 'DD/MM/YYYY HH:mm:ss:SSS').diff(
          dayjs(response.config.time, 'DD/MM/YYYY HH:mm:ss:SSS')
        )
      )
      .format('ss:SSS');

    console.log(
      '<<<<<< axios response <<<<<<\n %c' +
        response.config.method.toUpperCase() +
        '%c  %c ' +
        diff +
        ' ',
      'background: #f66; color: #fff',
      'background: #fff; color: #fff',
      'background: #4b49d7; color: #fff',
      response.config.url,
      '(' +
        dayjs(response.config.time).format('mm:ss:SSS') +
        ' ~ ' +
        dayjs(new Date()).format('mm:ss:SSS') +
        ')',
      '\ndata:',
      response.data
    );
    if (!response || !response.data?.common || !response.data?.common.success) {
      const comerr: any = response?.data?.common
        ? response?.data?.common?.error
        : '';

      console.error('!!!! api response fail !!!!\n', comerr);
    }

    // 로딩바 숨기기
    if (response.config.dispatch)
      response.config.dispatch('hideLoader', null, { root: true });

    // 로드 완료.
    fetching = false;

    return response;
  },
  function (error) {
    fetching = false;
    return Promise.reject(error);
  }
);

export { api, ajax, fetching };
